export default {
	state: {
		test: ''
  },
  mutations: {
		test (state) {
			state.test = 'yes!'	
		}
  },
  getters: {
	  getTest: state => state.test
  }
}